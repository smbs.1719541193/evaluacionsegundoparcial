﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            //Se crea una instancia y esta va a ser guardada en personaUno
            Singleton personaUno = Singleton.ObtenInstancia();

            //Aquí se están ingresando datos llamando al método y posteriormente se imprime
            personaUno.AsignarDatos("Sadana", "ecuatoriana", 19);
            Console.WriteLine(personaUno);
            Console.WriteLine("**********");

            //Es para realizar la verificación que se obtiene una única instancia
            //Se obtiene la instancia y esta se guarda en personaDos, pero esta nos va a regresar la misma instancia que teníamos en personaUno
            Singleton personaDos = Singleton.ObtenInstancia();

            //Mostramos por pantalla la instancia para comprobar que sea la misma
            Console.WriteLine(personaDos);
            Console.ReadKey();
        }
    }
}
