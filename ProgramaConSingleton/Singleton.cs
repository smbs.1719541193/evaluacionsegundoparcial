﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    class Singleton
    {
        //Se guarda la instancia única 
        private static Singleton instancia;

        //Datos que se van a agregar dentro de la clase
        private string nombre, nacionalidad;
        private int edad;
        
        //Constructor de Singleton privado, ya que este debe ser privado de manera forzosa
        //Como el constructor es privado no se puede acceder desde el exterior
        private Singleton()
        {
            nombre = "asignar nombre";
            nacionalidad = "asignar nacionalidad";
            edad = 128; 
        }
        //Se crea método para verificar que solamente exista una única instancia, además se crea una nueva instancia.
        public static Singleton ObtenInstancia()
        {
            //Si no existe la instancia va a ser nula, es por eso que se se compara dentro del if para conocer su existencia
            if (instancia == null) 
            {
                //Si no existe, se crea la instancia
                instancia = new Singleton();
                
                //Se muestra por pantalla que se ha creado una instancia por primera vez
                Console.WriteLine("  Instancia creada por primera vez   ");
            }

            //Se retorna la instancia
            return instancia;
        }

        //Métodos propios de la clase
        //Este método permite mostrar los datos
        public override string ToString()
        {
            return string.Format("La persona con nombre {0}, de nacionalidad {1}, tiene {2} años", nombre, nacionalidad, edad);

        }
        //Se usa este método para la asignación de datos desde el exterior
        public void AsignarDatos(string pNombre, string pNacionalidad, int pEdad)
        {
            nombre = pNombre;
            nacionalidad = pNacionalidad;
            edad = pEdad;
        }

    }

    
}
