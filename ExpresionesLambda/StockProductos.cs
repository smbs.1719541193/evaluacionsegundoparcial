﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarritoDeCompras
{
    public class StockProductos
    {
        public StockProductos()
        {
            this.ListaStockProductos = new List<Producto>();
        }

        public List<Producto> ListaStockProductos { get; set; }

        public void CrearProductos()
        {
            Random random = new Random();
            int numero;
            for (int i = 1; i <= 10; i++)
            {

                Producto producto = new Producto();
                producto.Identificador = i;
                numero = random.Next(20);
                char letra = (char)(((int)'A') + random.Next(26));
                producto.Descripcion = "PRODUCTO" + letra + numero + i;
                producto.Precio = numero;
                this.ListaStockProductos.Add(producto);
            }
        }

        public void ImprimirStockProductos()
        {
            Console.WriteLine("Stock de Productos");
            Console.WriteLine("Identificador\tDescripción\tPrecio");
            //Se hace el uso de expresion lambda para crear una lista donde esté la información de los productos del Stock.
            var ListaProductos = ListaStockProductos.Select(t => t); 
            foreach (var item in ListaProductos)
            {
                Console.WriteLine("{0}\t\t{1}\t{2}",
                item.Identificador, item.Descripcion, item.Precio);

            }
        }
        //Se crea método para buscar un producto por el identificador.
        public void BusquedaDeObjeto()
        {
            int busca;
            Console.WriteLine("Ingrese el identificador del producto que desea buscar");
            busca = int.Parse(Console.ReadLine());
            Console.WriteLine("El resultado de la búsqueda ");
            Console.WriteLine("Identificador\tDescripción\tPrecio");
            //Expresion lambda que permita realizar la búsqueda del producto particular dentro de la lista.
            var ProductoBuscado = ListaStockProductos.Where(b => b.Identificador == busca);
            foreach (var item in ProductoBuscado)
            {
                Console.WriteLine("{0}\t\t{1}\t{2}",
                item.Identificador, item.Descripcion, item.Precio);

            }
        }
        //Se crea método para Ordenar el precio del producto de menor a mayor
        public void OrdenarProductos()
        {
            Console.WriteLine("Productos ordenados de menor a mayor");
            Console.WriteLine("Identificador\tDescripción\tPrecio");
            // Expresion lambda para ordenar los precios de forma ascendente.
            var ListaOrdenada = ListaStockProductos.OrderBy(o => o.Precio);
            foreach (var item in ListaOrdenada)
            {
                Console.WriteLine("{0}\t\t{1}\t{2}",
                item.Identificador, item.Descripcion, item.Precio);

            }
        }

        //Se crea método para mostrar el producto con el precio más alto.

        public void ProductoPrecioMayor()
        {
            Console.WriteLine("Producto cuyo  precio es mayor");
            Console.WriteLine("Identificador\tDescripción\tPrecio");
            // Expresion lambda para ordenar los precios de forma descendente.
            var PrecioMayor = ListaStockProductos.OrderByDescending(o => o.Precio);
            int contador = 0;
            decimal mayor = 0 ; 
            foreach (var item in PrecioMayor)
            {
                if (contador == 0)
                {
                    mayor = item.Precio;
                    Console.WriteLine("{0}\t\t{1}\t{2}",
                    item.Identificador, item.Descripcion, item.Precio);
                    contador++;
                }
                else
                {
                    if (mayor == item.Precio)
                    {
                        Console.WriteLine("{0}\t\t{1}\t{2}",
                        item.Identificador, item.Descripcion, item.Precio);
                    }
                }

            }
            
        }
    }
}
