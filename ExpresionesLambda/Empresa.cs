﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarritoDeCompras
{
    public class Empresa
    {
        public Empresa()
        {
        }

        public string RazonSocial { get; set; }
        public string Direccion { get; set; }
    }
}
